﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UI;

public class DragonTooth : InteractableItem
{
    public enum TEETH_STATES {NORMAL, CRACKED, BROKEN, INGOT, REPAIRED }
    public Animator animator;
    public TEETH_STATES currentState = TEETH_STATES.NORMAL;
    public Image imgFillBar;
    public float maxRepair;
    public float currentRepair;
    public RectTransform UiParent;

    private void Awake()
    {
        ChangeState(TEETH_STATES.NORMAL);
    }

    public override bool CanBeInteractedWith()
    {
        return false;
    }

    public override void Interact(PlayerCharacter c)
    {
        base.Interact(c);
        Debug.Log("DragonTeethInteracted");
        transform.DOPunchScale(Vector3.one * 0.2f, 0.2f);
        if (currentState == TEETH_STATES.INGOT)
        {
            AdvanceRepair();
        }
        else
        { 
            //NOTHING
        }
    }

    public void OnBulletHit(KillerBullet killerBullet)
    {
        if (killerBullet.scrapRemaining > 0)
        {
            Damage();
        }
    }

    public void OnPlayerApproachesWithIngot(PlayerCharacter c)
    {
        if (currentState == TEETH_STATES.CRACKED)
        {
            animator.SetTrigger("INGOT");
            ChangeState(TEETH_STATES.INGOT);
            c.ClaimIngot();
        }
    }

    public void AdvanceRepair()
    {
        currentRepair += (maxRepair / 4f);
        imgFillBar.fillAmount = currentRepair / maxRepair;
        if (imgFillBar.fillAmount == 1)
        {
            Repair();
        }
    }

    public void Repair()
    {
        animator.SetTrigger("REPAIR");
        ChangeState(TEETH_STATES.REPAIRED);
    }

    public void Damage()
    {
        animator.SetTrigger("DAMAGE");
        switch (currentState)
        {
            case TEETH_STATES.NORMAL:
                ChangeState(TEETH_STATES.CRACKED);
                break;
            case TEETH_STATES.CRACKED:
                ChangeState(TEETH_STATES.BROKEN);
                break;
            case TEETH_STATES.BROKEN:
                break;
            case TEETH_STATES.INGOT:
                ChangeState(TEETH_STATES.CRACKED);
                ShowUIParent();
                break;
            case TEETH_STATES.REPAIRED:
                ChangeState(TEETH_STATES.CRACKED);
                break;
        } 
    }

    void ShowUIParent()
    {
        UiParent.transform.position = transform.position + transform.forward*2f + Vector3.up * 3f;
        UiParent.localScale = Vector3.one * 0.01f;
        UiParent.gameObject.SetActive(true);
        UiParent.DOScale(Vector3.one * 1f, 0.4f).SetEase(Ease.OutBack);
    }

    public void ChangeState(TEETH_STATES st)
    {
        currentState = st;
        if (currentState == TEETH_STATES.INGOT)
        {
            ShowUIParent();
        }
        else
        {
            UiParent.gameObject.SetActive(false);
        }
    }    
}