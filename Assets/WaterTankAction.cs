﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTankAction : MonoBehaviour
{
    public ParticleSystem ps;
    private bool checkWater;

    private void OnEnable()
    {
        ps = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        checkWater = ps.isPlaying;
    }

    private void OnTriggerStay(Collider other)
    {
        if (checkWater)
        {
            if (other.GetComponent<BurnableItem>() != null)
            {
                var b = other.GetComponent<BurnableItem>();
                b.Douse();
            }
        }
    }
}
