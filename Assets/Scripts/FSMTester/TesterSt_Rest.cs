﻿using UnityEngine;
using System.Collections;

public class TesterSt_Rest : State<FSMTester> 
{
	public override void Enter (FSMTester owner)
	{
		Debug.Log("Enter state rest");
	}

	public override void Execute (FSMTester owner)
	{
		owner.Rest();
	}

	public override void Exit (FSMTester owner)
	{
		Debug.Log("Exit state rest");
	}
}
