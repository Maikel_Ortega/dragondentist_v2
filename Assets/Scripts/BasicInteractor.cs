﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BasicInteractor : InteractableItem
{
    public override bool CanBeInteractedWith()
    {
        return base.CanBeInteractedWith();
    }

    public override void Interact(PlayerCharacter c)
    {
        base.Interact(c);
        Debug.Log("ITEM NAME IS: " + gameObject.name);
        DOTween.Kill(transform);
        transform.DOPunchScale(Vector3.one * 0.3f, 0.4f).SetEase(Ease.InOutCubic);
    }
}
