﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnableItem : MonoBehaviour
{
    public bool burning = false;
    private GameObject burningFX;

    public bool IsBurning()
    {
        return burning;
    }

    private void Start()
    {
        burningFX = Instantiate(GameManager.Instance.burningFX);
        burningFX.transform.SetParent(transform);
        burningFX.transform.localPosition = Vector3.zero;
        burningFX.gameObject.SetActive(false);
    }

    private void Update()
    {
        ShowBurningEffects(IsBurning());        
    }

    public void Douse()
    {
        burning = false;
        //douseFX
    }

    private void ShowBurningEffects(bool v)
    {
        burningFX.gameObject.SetActive(v);
    }
}
