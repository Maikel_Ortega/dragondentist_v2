﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class StunCountdown : MonoBehaviour
{
    public event System.Action OnCountdownFinished;
    public List<Sprite> bgByPlayerNumber;
    public TextMeshProUGUI text;
    public UnityEngine.UI.Image img;

    public void StartCountdown(int pNumber)
    {
        img.gameObject.SetActive(true);

        img.sprite = bgByPlayerNumber[pNumber - 1];
        StartCoroutine(Countdown());
    }

    void ChangeNumber(int i)
    {
        text.text = i.ToString();
        img.rectTransform.DOPunchScale(Vector3.one*0.3f,0.5f).SetEase(Ease.OutElastic);

    }

    void FinishCountdown()
    {
        Sequence s = DOTween.Sequence();
        s.Append(img.rectTransform.DOScale(Vector3.one * 0.01f, 0.4f).SetEase(Ease.InBack));        
        s.OnComplete(() 
            => { 
                img.gameObject.SetActive(false);
                img.rectTransform.localScale = Vector3.one;
                OnCountdownFinished();
            });        
    }

    IEnumerator Countdown()
    {
        for (int i = 5; i > 0; i--)
        {
            ChangeNumber(i);
            yield return new WaitForSeconds(1f);    
        }
        FinishCountdown();
    }
}
