﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool_Hammer : Tool
{
    public Collider boxCastCollider;
    public LayerMask dragonToothFilter;
    float m_MaxDistance = 3.0f;
    RaycastHit info;
    bool hitDetect;
    Vector3 lastHit;
    public override void Use()
    {
        base.Use();
        owner.UseHammerAnimation();
        hitDetect = Physics.BoxCast(transform.position, boxCastCollider.transform.localScale, transform.forward, out info, boxCastCollider.transform.rotation, m_MaxDistance, dragonToothFilter);
        if (hitDetect)
        {
            Debug.Log("HIT SOMETHING" + info.collider.gameObject.name);
            var dt = info.collider.GetComponentInChildren<DragonTooth>();
            dt.Interact(owner);
            lastHit = info.point;
        }

    }

    public override void PickUp(PlayerCharacter c)
    {
        base.PickUp(c);
        c.armRenderer.sprite = c.hammerArm;
        GetComponentInChildren<SpriteRenderer>().enabled = false;
    }

    public override void Drop(PlayerCharacter c)
    {
        base.Drop(c);
        c.armRenderer.sprite = c.normalArm;
        GetComponentInChildren<SpriteRenderer>().enabled = true;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        //Check if there has been a hit yet
        if (hitDetect)
        {
            Gizmos.color = Color.green;

            //Draw a Ray forward from GameObject toward the hit
            Gizmos.DrawRay(transform.position, transform.forward * info.distance);
            //Draw a cube that extends to where the hit exists
            Gizmos.DrawWireCube(transform.position + transform.forward * info.distance, boxCastCollider.transform.localScale);
        }
        //If there hasn't been a hit yet, draw the ray at the maximum distance
        else
        {
            //Draw a Ray forward from GameObject toward the maximum distance
            Gizmos.DrawRay(transform.position, transform.forward * m_MaxDistance);
            //Draw a cube at the maximum distance
            Gizmos.DrawWireCube(transform.position + transform.forward * m_MaxDistance, boxCastCollider.transform.localScale);
        }
        Gizmos.DrawWireSphere(lastHit, 0.1f);
    }
}
