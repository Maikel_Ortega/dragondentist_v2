﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[System.Serializable]
public struct PickableScrapConfig
{
    public Sprite spr;
    public float stuckZRotation;
    public Vector2 stuckOffset;
    public float heldZRotation;
    public Vector2 heldOffset;
    public bool isMaterial;
}

public class BasicPickable : PickableItem
{
    public bool isMaterial = false;
    public List<PickableScrapConfig> possibleConfigs;
    public PickableScrapConfig ingotConfig;
    private PickableScrapConfig currentConfig;
    public Vector2 minMaxRandomRot;
    [HideInInspector()]
    public SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        currentConfig = possibleConfigs[Random.Range(0, possibleConfigs.Count)];
        SetSpriteAndRotation(spriteRenderer);
    }

    public void SetIngot()
    {
        currentConfig = ingotConfig;
        SetSpriteAndRotation(spriteRenderer);
    }

    public void SetSpriteAndRotation(SpriteRenderer spr, bool stuck = true)
    {
        float rot = 0;
        Vector2 offset;
        Sprite newSprite = currentConfig.spr;
        if (stuck)
        {
            rot = currentConfig.stuckZRotation;
            offset = currentConfig.stuckOffset;
        }
        else
        {
            rot = currentConfig.heldZRotation;
            offset = currentConfig.heldOffset;
        }

        rot += Random.Range(minMaxRandomRot.x, minMaxRandomRot.y);
        spr.sprite = newSprite;
        spr.transform.localRotation = Quaternion.Euler(0,0,rot);
        spr.transform.localPosition = Vector3.zero;
        spr.transform.localPosition += (Vector3)offset;
        isMaterial = currentConfig.isMaterial;

    }

    public override void PickUp(PlayerCharacter c)
    {
        base.PickUp(c);
        SetSpriteAndRotation(c.scrapHolder, false);
        spriteRenderer.enabled = false;
    }

    public override void Drop(PlayerCharacter c)
    {
        base.Drop(c);
        c.scrapHolder.sprite = null;
        spriteRenderer.enabled = true;
    }
}
