﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    public int playerNumber;
    public float speed = 10f;

    private CharacterController controller;
    
    public StateMachine<PlayerCharacter> fsm;

    public State<PlayerCharacter> St_Normal;
    public State<PlayerCharacter> St_CarryingCargo;
    public State<PlayerCharacter> St_CarryingTool;
    public State<PlayerCharacter> St_Using;
    public State<PlayerCharacter> St_Shield;
    public State<PlayerCharacter> St_Stun;

    public Animator characterAnimator;
    public SpriteRenderer characterSprite;
    public PlayerInteractor interactor;    
    public PickableItem carriedItem;
    public Transform directionPointer;
    public StunCountdown stunCountdown;
    public SpriteRenderer scrapHolder;
    public GameObject flipParent;

    public Sprite waterArm;
    public Sprite hammerArm;
    public Sprite normalArm;
    public SpriteRenderer armRenderer;

    public GameObject tankObject;


    int dir = 1;
    Color mColor;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
        St_Normal = new PlayerSt_Normal();
        St_CarryingCargo = new PlayerSt_Cargo();
        St_CarryingTool = new PlayerSt_Tool();
        St_Using = new PlayerSt_UsingTool();
        St_Stun = new PlayerSt_Stun();
        St_Shield = new PlayerSt_Shield();
        fsm = new StateMachine<PlayerCharacter>(this, St_Normal);
    }

    public void SetupPlayer()
    {
        mColor = GameManager.Instance.GetColor(playerNumber);
        characterAnimator.runtimeAnimatorController = GameManager.Instance.GetSkin(playerNumber);
        interactor.SetColor(mColor);
        interactor.OnEnterDragonToothArea += Interactor_OnEnterDragonToothArea;
        foreach (var item in directionPointer.GetComponentsInChildren<SpriteRenderer>())
        {
            item.color = mColor;
        }
    }

    private void Interactor_OnEnterDragonToothArea(GameObject obj)
    {
        if (fsm.IsInState(St_CarryingCargo) && carriedItem.GetComponent<BasicPickable>() != null && carriedItem.GetComponent<BasicPickable>().isMaterial)
        {
            obj.GetComponent<DragonTooth>().OnPlayerApproachesWithIngot(this);
        }
    }

    public void ClaimIngot()
    {
        carriedItem.Drop(this);
        fsm.ChangeState(St_Normal);
        Destroy(carriedItem.gameObject);
    }

    private void Update()
    {
        fsm.DoUpdate();
        if (Input.GetKeyDown(KeyCode.Q))
        {
            OnDamageReceived();
        }
    }

    public void CheckMovement()
    {
        Vector2 input = new Vector2(GameManager.Instance.input.GetAxis(playerNumber, PlayerConfigScriptableData.PLAYER_INPUT_AXIS.HORIZONTAL_AXIS)
            , GameManager.Instance.input.GetAxis(playerNumber, PlayerConfigScriptableData.PLAYER_INPUT_AXIS.VERTICAL_AXIS));
                
        //TODO: Make movement according to camera        
        Vector3 movement = InputCameraTransform(input);

        bool walking = false;
        if (movement.magnitude > 0.1f)
        {
            walking = true;
            SetPointer(movement);
        }
        SetDirection(movement);      
        characterAnimator.SetBool("WALKING", walking);
        controller.Move(movement*Time.deltaTime*speed);
    }

    private void SetDirection(Vector3 m)
    {
        if (m.x > 0 && dir < 0)
        {
            dir = 1;
            characterSprite.flipX = false;
            flipParent.transform.localScale = new Vector3(1,1,1);
        }
        else if (m.x < 0 && dir > 0)
        {
            dir = -1;
            characterSprite.flipX = true;
            flipParent.transform.localScale = new Vector3(-1,1,1);
        }
    }

    private void SetPointer(Vector3 movement)
    {
        var newRot = Quaternion.LookRotation(movement, Vector3.up);
        directionPointer.transform.rotation = newRot;
    }

    private Vector3 InputCameraTransform(Vector2 input)
    {
        return new Vector3(input.x, 0, input.y);
    }

    public bool IsInteractInputDown()
    {
        return GameManager.Instance.input.GetButtonDown(playerNumber, PlayerConfigScriptableData.PLAYER_INPUT_BUTTON.INTERACT_BUTTON);
    }

    public bool IsDropInputDown()
    {
        return GameManager.Instance.input.GetButtonDown(playerNumber, PlayerConfigScriptableData.PLAYER_INPUT_BUTTON.CANCEL_BUTTON);
    }

    public void CheckPickable()
    {
        if (interactor.selectedItem != null && interactor.selectedItem is PickableItem)
        {
            var item = (interactor.selectedItem as PickableItem);
            if (item.CanBeInteractedWith())
            { 
                item.Interact(this);
            }
        }
    }

    public void PickUpItem(PickableItem pickableItem)
    {
        carriedItem = pickableItem;
        switch (pickableItem.pickableType)
        {
            case PickableItem.PICKABLE_ITEM_TYPES.CARGO:
                fsm.ChangeState(St_CarryingCargo);
                break;
            case PickableItem.PICKABLE_ITEM_TYPES.TOOL:
                fsm.ChangeState(St_CarryingTool);
                break;
        }
    }

    public void ActivateShield(bool shield)
    {
        characterAnimator.SetBool("SHIELD", shield);
    }

    public void CheckShieldInputStarts()
    {
        if (GameManager.Instance.input.GetButtonDown(playerNumber, PlayerConfigScriptableData.PLAYER_INPUT_BUTTON.SHIELD_BUTTON))
        {
            fsm.ChangeState(St_Shield);
        }
    }

    public void CheckShieldInputEnded()
    {
        if (!GameManager.Instance.input.GetButton(playerNumber, PlayerConfigScriptableData.PLAYER_INPUT_BUTTON.SHIELD_BUTTON))
        {
            fsm.ChangeState(St_Normal);
        }
    }

    public void CheckInteract()
    {
        if (interactor.selectedItem != null)
        {
            interactor.selectedItem.Interact(this);
        }
    }

    public void CheckTool()
    {
        (carriedItem as Tool).Use();
    }

    public void CheckDrop()
    {
        carriedItem.Drop(this);
        fsm.ChangeState(St_Normal);
    }

    public void ActivateInteractor(bool active)
    {
        interactor.Activate(active);
    }

    
    public void UseHammerAnimation()
    {
        characterAnimator.SetBool("HAMMER", true);
        UseTool(0.5f);
    }

    public void UseTool(float duration)
    {
        fsm.ChangeState(St_Using);        
        StartCoroutine(ChangeBackToPreviousState(duration));
    }

    IEnumerator ChangeBackToPreviousState(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        fsm.RevertToPreviousState();
        characterAnimator.SetBool("HAMMER", false);
    }

    public void OnBulletHit(KillerBullet killerBullet)
    {
        if (!fsm.IsInState(St_Stun))
        { 
            OnDamageReceived();
        }
    }

    public void OnDamageReceived()
    {
        fsm.ChangeState(St_Stun);
    }


    public void StartStun()
    {
        StopAllCoroutines();
        characterAnimator.SetBool("STUN", true);
        stunCountdown.OnCountdownFinished += OnCountdownFinished;
        stunCountdown.StartCountdown(playerNumber);
    }

    void OnCountdownFinished()
    {
        fsm.ChangeState(St_Normal);
    }

    public void OnStunEnds()
    {
        characterAnimator.SetBool("STUN", false);
    }
}

//----------------

public class PlayerSt_Normal : State<PlayerCharacter>
{
    public override void Enter(PlayerCharacter owner)
    {
        
    }

    public override void Execute(PlayerCharacter owner)
    {
        owner.CheckShieldInputStarts();
        owner.CheckMovement();
        if (owner.IsInteractInputDown())
        { 
            owner.CheckInteract();            
        }
    }

    public override void Exit(PlayerCharacter owner)
    {
        
    }
}

public class PlayerSt_Cargo : State<PlayerCharacter>
{
    public override void Enter(PlayerCharacter owner)
    {
        owner.ActivateInteractor(false);
    }

    public override void Execute(PlayerCharacter owner)
    {
        owner.CheckMovement();
        if (owner.IsDropInputDown())
        {
            owner.CheckDrop();
        }
    }

    public override void Exit(PlayerCharacter owner)
    {
        owner.ActivateInteractor(true);

    }
}


public class PlayerSt_UsingTool : State<PlayerCharacter>
{
    public override void Enter(PlayerCharacter owner)
    {
    }

    public override void Execute(PlayerCharacter owner)
    {
        
    }

    public override void Exit(PlayerCharacter owner)
    {
    }
}


public class PlayerSt_Tool : State<PlayerCharacter>
{
    public override void Enter(PlayerCharacter owner)
    {
        owner.ActivateInteractor(false);
    }

    public override void Execute(PlayerCharacter owner)
    {
        owner.CheckMovement();
        if (owner.IsInteractInputDown())
        {
            owner.CheckTool();
        }
        if (owner.IsDropInputDown())
        {
            owner.CheckDrop();
        }
    }

    public override void Exit(PlayerCharacter owner)
    {
        owner.ActivateInteractor(true);
    }
}

public class PlayerSt_Stun : State<PlayerCharacter>
{
    public override void Enter(PlayerCharacter owner)
    {
        owner.StartStun();
        owner.ActivateInteractor(false);

    }

    public override void Execute(PlayerCharacter owner)
    {
       
    }

    public override void Exit(PlayerCharacter owner)
    {
        owner.OnStunEnds();
        owner.ActivateInteractor(true);
    }
}


public class PlayerSt_Shield : State<PlayerCharacter>
{
    public override void Enter(PlayerCharacter owner)
    {
        owner.ActivateShield(true);

    }

    public override void Execute(PlayerCharacter owner)
    {
        owner.CheckShieldInputEnded();
    }

    public override void Exit(PlayerCharacter owner)
    {        
        owner.ActivateShield(false);
    }
}