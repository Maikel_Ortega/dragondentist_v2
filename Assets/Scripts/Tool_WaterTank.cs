﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool_WaterTank : Tool
{
    public ParticleSystem waterParticles;
    public override void Use()
    {
        base.Use();
        waterParticles.Play();
    }


    public override void PickUp(PlayerCharacter c)
    {
        base.PickUp(c);
        c.armRenderer.sprite = c.waterArm;
        c.tankObject.SetActive(true);
        GetComponentInChildren<SpriteRenderer>().enabled = false;

    }

    public override void Drop(PlayerCharacter c)
    {
        base.Drop(c);
        c.armRenderer.sprite = c.normalArm;
        c.tankObject.SetActive(false);
        GetComponentInChildren<SpriteRenderer>().enabled = true;

    }
}
