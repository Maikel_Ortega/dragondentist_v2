﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerBullet : MonoBehaviour
{
    public float speed = 10f;
    public int scrapRemaining = 0;

    public Vector2 minMaxScrapCounter;
    private float scrapCounter;
    public GameObject scrapPrefab;

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<PlayerCharacter>();
        if (player != null)
        {
            player.OnBulletHit(this);
        }
        var dragonTooh = other.GetComponent<DragonTooth>();
        if (dragonTooh != null)
        {
            dragonTooh.OnBulletHit(this);
        }
        scrapCounter = Random.Range(minMaxScrapCounter.x, minMaxScrapCounter.y);
    }

    void Update()
    {
        transform.Translate(transform.forward * speed * Time.deltaTime,Space.World);
        if (scrapRemaining > 0)
        {
            UpdateScrap();
        }
    }

    public void UpdateScrap()
    {
        scrapCounter -= Time.deltaTime;
        if (scrapCounter <= 0)
        {
            LeaveScrap();
            scrapCounter = Random.Range(minMaxScrapCounter.x, minMaxScrapCounter.y);
        }
    }

    void LeaveScrap()
    {
        Instantiate(scrapPrefab, transform.position, Quaternion.identity);
        scrapRemaining--;
    }
}
