﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HazardLine : MonoBehaviour
{
    public StateMachine<HazardLine> fsm;
    public State<HazardLine> stEmpty;
    public State<HazardLine> stWarning;
    public State<HazardLine> stAttack;
    public SpriteRenderer warningBeam;
    public ParticleSystem hazardAttackParticles;
    public Sequence hazardSequence;
    private BulletShooter shooter;
    public GameObject scrapPrefab;
    public Vector2 minMaxScraps;
    public float maxCounter = 0.5f;
    private float counter = 0.5f;
    private int scrapRemainingPerLine = 0;    

    private void Awake()
    {
        stWarning = new HazardLine_StWarning();
        stAttack = new HazardLine_StAttack();
        stEmpty = new HazardLine_StEmpty();
        fsm = new StateMachine<HazardLine>(this, stEmpty);
        shooter = GetComponent<BulletShooter>();
        counter = maxCounter;
    }

    public void Update()
    {
        fsm.DoUpdate();
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            StartWarning();
        }
    }

    public void UpdateAttackTimer()
    {
        counter -= Time.deltaTime;
        if (counter <= 0)
        {            
            shooter.Shoot(scrapRemainingPerLine);
            scrapRemainingPerLine = 0;
            counter = maxCounter;
        }
    }

    public void StartWarning(float warningSeconds = 3f)
    {
        fsm.ChangeState(stWarning);
        StartCoroutine(ChangeToStateInSeconds(warningSeconds, stAttack));
    }

    public void StartAttack(float duration = 1f)
    {        
        StartCoroutine(ChangeToStateInSeconds(duration, stEmpty));

        scrapRemainingPerLine = Random.Range((int)minMaxScraps.x, (int)minMaxScraps.y);
        counter = 0f;
        warningBeam.transform.DOPunchScale(new Vector3(1, 0, 1) * 1, 1);
    }

    IEnumerator ChangeToStateInSeconds(float seconds, State<HazardLine> state)
    {
        yield return new WaitForSeconds(seconds);
        fsm.ChangeState(state);
    }
}



public class HazardLine_StEmpty: State<HazardLine>
{
    public override void Enter(HazardLine owner)
    {
        owner.warningBeam.DOFade(0, 0.1f);
    }

    public override void Execute(HazardLine owner)
    {

    }

    public override void Exit(HazardLine owner)
    {

    }
}
public class HazardLine_StWarning : State<HazardLine>
{
    public override void Enter(HazardLine owner)
    {
        owner.hazardSequence= DOTween.Sequence();
        owner.hazardSequence.Append(owner.warningBeam.DOFade(0.2f, 0.5f));
        owner.hazardSequence.Append(owner.warningBeam.DOFade(0.75f, 0.5f));
        owner.hazardSequence.SetLoops(-1);
    }

    public override void Execute(HazardLine owner)
    {

    }

    public override void Exit(HazardLine owner)
    {
        owner.hazardSequence.Kill();
    }
}
public class HazardLine_StAttack : State<HazardLine>
{
    public override void Enter(HazardLine owner)
    {
        owner.StartAttack(2);
        owner.hazardAttackParticles.Play();
    }

    public override void Execute(HazardLine owner)
    {
        owner.UpdateAttackTimer();
    }

    public override void Exit(HazardLine owner)
    {
        owner.hazardAttackParticles.Stop();
    }
}


