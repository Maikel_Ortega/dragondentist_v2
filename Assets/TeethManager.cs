﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeethManager : MonoBehaviour
{
    public List<GameObject> dragonTeethDummyGO;
    public DragonTooth dragonToothPrefab;

    void Awake()
    {
        SearchForDragonTeeth();
    }

    public void SearchForDragonTeeth()
    {
        dragonTeethDummyGO = new List<GameObject>(GameObject.FindGameObjectsWithTag("dragonTeeth"));
        foreach (var item in dragonTeethDummyGO)
        {
            var go = Instantiate(dragonToothPrefab.gameObject, item.transform);
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            go.transform.localRotation = Quaternion.identity;
        }
    }
}
