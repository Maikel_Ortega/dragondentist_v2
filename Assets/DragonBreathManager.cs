﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DragonBreathManager : MonoBehaviour
{
    public ParticleSystem breath;
    public Light breathLight;
    public float breathDuration = 3f;
    public float minLight = 6f;
    public float maxLight = 12f;
    Sequence s;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            DragonBreath();
        }
    }

    void ActivateBreath(bool active)
    {
        if (active)
        {
            breath.Play();
            breathLight.enabled = true;
            s = DOTween.Sequence();
            s.Append(breathLight.DOIntensity(minLight, 0.5f));
            s.Append(breathLight.DOIntensity(maxLight, 0.6f).SetEase(Ease.InCubic)).SetLoops(-1, LoopType.Yoyo);
            s.Play();
        }
        else
        {
            breath.Stop();
            s.Kill();
            breathLight.DOIntensity(0f, 0.5f);
        }
    }

    public void DragonBreath()
    {
        StartCoroutine(DragonBreathCoroutine());
    }

    IEnumerator DragonBreathCoroutine()
    {
        ActivateBreath(true);
        yield return new WaitForSeconds(breathDuration);
        ActivateBreath(false);
    }
}
