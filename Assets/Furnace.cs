﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Furnace : MonoBehaviour
{
    private StateMachine<Furnace> fsm;
    private State<Furnace> stIdle;
    private State<Furnace> stWorking;
    public float expulsionForce = 10f;
    private BasicPickable currentIngot;
    void Awake()
    {
        stIdle = new FurnaceSt_Idle();
        stWorking = new FurnaceSt_Working();
        fsm = new StateMachine<Furnace>(this, stIdle);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    IEnumerator ChangeBackToPreviousState(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        fsm.RevertToPreviousState();
    }


    private void OnTriggerEnter(Collider other)
    {
        bool rejected = false;
        if (fsm.IsInState(stIdle))
        {
            if (other.GetComponentInChildren<BasicPickable>() && !other.GetComponentInChildren<BasicPickable>().isMaterial)
            {
                var pick = other.GetComponentInChildren<BasicPickable>();
                currentIngot = pick;
                currentIngot.gameObject.SetActive(false);
                fsm.ChangeState(stWorking);
                StartCoroutine(ChangeBackToPreviousState(1f));
            }
            else
            {
                rejected = true;
            }
        }
        else
        {
            rejected = true;
        }


        if(rejected && other.GetComponentInChildren<Rigidbody>()!= null)
        {
            var rb = other.GetComponentInChildren<Rigidbody>();
            Vector2 dir = other.transform.position - transform.position;
            Vector3 expelVector = rb.velocity.normalized * -1;
            expelVector = expelVector.normalized;
            rb.AddForce(expelVector* expulsionForce, ForceMode.Impulse);
        }
    }

    public void ExpelIngot()
    {
        currentIngot.transform.position = transform.position + Vector3.up * 2;
        currentIngot.SetIngot();
        currentIngot.gameObject.SetActive(true);
        var rb = currentIngot.GetComponent<Rigidbody>();
        Vector3 expelVector = new Vector3(0, 0.5f, -1).normalized;
        rb.velocity =(expelVector*expulsionForce);
        currentIngot = null;
    }
}

public class FurnaceSt_Idle : State<Furnace>
{
    public override void Enter(Furnace owner)
    {
    }

    public override void Execute(Furnace owner)
    {
    }

    public override void Exit(Furnace owner)
    {
    }
}
public class FurnaceSt_Working : State<Furnace>
{
    public override void Enter(Furnace owner)
    {        
    }

    public override void Execute(Furnace owner)
    {
    }

    public override void Exit(Furnace owner)
    {
        owner.ExpelIngot();
    }
}