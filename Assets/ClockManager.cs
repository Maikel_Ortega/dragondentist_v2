﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ClockManager : MonoBehaviour
{
    public float currentSeconds = 180;
    public float totalSeconds = 180;
    public bool active;
    public Image fillBarImage;
    public TextMeshProUGUI text;
    public RectTransform particlesTransform;
    public Vector2 minMaxPos;
    private void Update()
    {
        if (active)
        { 
            currentSeconds -= Time.deltaTime;
            fillBarImage.fillAmount = currentSeconds / totalSeconds;
            text.text = GetSecondsToText(Mathf.Ceil(currentSeconds));
            float newX = Mathf.Lerp(minMaxPos.x, minMaxPos.y, fillBarImage.fillAmount);
            particlesTransform.anchoredPosition = new Vector2(newX, 0);
        }
    }

    string GetSecondsToText(float seconds)
    {
        TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);
        return string.Format("{0:D2}:{1:D2}",  timeSpan.Minutes, timeSpan.Seconds);
    }
}
